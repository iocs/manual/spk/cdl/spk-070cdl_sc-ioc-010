###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: Cryo_FIS                                                             ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt:   opis/acc/cryo/engineer           ##
##                                                                                          ##  
##    FS - Flow switch with two possible digital inputs both normally open WF and RF        ##
##                                                                                          ##  
##    Created based on ICS_FIS version 1.3                                                  ##  
############################         Version: 1.3             ################################
# Author:  Wojciech Bińczyk
# Date:    26-03-2024
# Version: v1.0
##############################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()

#Operation modes
add_digital("OpMode_FreeRun",          ARCHIVE=True,                    PV_DESC="Operation Mode FreeRun", PV_ONAM="True",                     PV_ZNAM="False")
add_digital("OpMode_Forced",           ARCHIVE=True,                    PV_DESC="Operation Mode Forced",  PV_ONAM="True",                     PV_ZNAM="False")

#Transmitter value
add_digital("Flow_OK_RF",              ARCHIVE=True,                    PV_DESC="Flow above RF limit",             PV_ONAM="True",            PV_ZNAM="False")
add_digital("Flow_OK_WF",              ARCHIVE=True,                    PV_DESC="Flow above WF limit",             PV_ONAM="True",            PV_ZNAM="False")
add_digital("DI_Flow_OK_RF",           ARCHIVE=True,                    PV_DESC="Input Flow RF limit",             PV_ONAM="True",            PV_ZNAM="False")
add_digital("DI_Flow_OK_WF",           ARCHIVE=True,                    PV_DESC="Input Flow WF limit",             PV_ONAM="True",            PV_ZNAM="False")

#Alarm signals
add_major_alarm("GroupAlarm",          "GroupAlarm",                     PV_ONAM="Error",            PV_ZNAM="NominalState")
add_major_alarm("IO_Error",            "Error in device",                PV_ONAM="Error",            PV_ZNAM="NominalState")
add_major_alarm("HW_ModuleError",      "Module_Error",                   PV_ONAM="Error",            PV_ZNAM="NominalState")

############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_FreeRun",             PV_DESC="CMD: FreeRun Mode")
add_digital("Cmd_Force",               PV_DESC="CMD: Force Mode")

add_digital("Cmd_Flow_ON_RF",          PV_DESC="CMD: Force Value")
add_digital("Cmd_Flow_OFF_RF",         PV_DESC="CMD: Force Value")

add_digital("Cmd_Flow_ON_WF",          PV_DESC="CMD: Force Value")
add_digital("Cmd_Flow_OFF_WF",         PV_DESC="CMD: Force Value")

add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()


